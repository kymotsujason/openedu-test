Feature: News article(s)

  @javascript
  Scenario: Accessing the site news articles anonymously
    Given I am an anonymous user
    When I visit "/news"
    Then I should see "In The News"
    And I should see "The Latest from OpenEDU News"

  @javascript
  Scenario: Searching the site news articles anonymously
    Given I am an anonymous user
    When I visit "/news"
    And I fill in "search" with "Students"
    And I press "Search"
    And I wait 5 seconds
    Then I should not see "choices"
