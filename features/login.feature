@api
Feature: Logging in and out

  @javascript
  Scenario: Logging in as admin
    Given I am an anonymous user
    When I visit "/user/login"
    Then I should see "Username"
    And I should see "Password"
    And I fill in "name" with "kymotsujason"
    And I fill in "pass" with "123123123"
    And I press "op"
    And I should see "3 hours"

  @javascript
  Scenario: Logging out as admin
    Given I am logged in as a user with the administrator role
    When I visit "/user/1"
    Then I should see "3 hours"
    And I press "Log out"
    And I should see "Explore OpenEDU"